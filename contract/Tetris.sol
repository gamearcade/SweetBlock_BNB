// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}

abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() {
        _transferOwnership(_msgSender());
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        _transferOwnership(newOwner);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Internal function without access restriction.
     */
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}

contract Tetris is Ownable{

    //Account Structure for their own game
    struct accountGame {
        address connectedAccount1;
        address connectedAccount2;
        uint256 betAmount;
    }

    //Store the accounts as the game ID
    mapping(uint => accountGame) public gameControl;

    //Store the amount following address to the betBNBamount
    mapping(address => uint256) public betBNBamount;

    //Using isbetting mapping for the flag of the bet performance
    mapping(address => bool) public isbetting;

    //Contract comision
    uint16 feePercent = 10;

    //Event return token to the Owner
    event ReturnToOwner(uint256 amount);

    //Event return token to the Winner
    event WithDraw(address winner, uint256 amount);

    //transaction successfully performed and receive BNB
    event Received (address , uint);

    //Return the GameState as the GameID
    function getGameState(uint gameID) onlyOwner public view returns(address account1, address account2, uint256 amount ) {
        accountGame memory accountGameById = gameControl[gameID];
        return (accountGameById.connectedAccount1, accountGameById.connectedAccount2, accountGameById.betAmount);
    }
    
    //Create the Game and Set GameID
    function createGame(uint gameID, address account1, address account2) onlyOwner public returns(bool) {
        accountGame memory newGame = accountGame(account1, account2, 0);
        gameControl[gameID] = newGame;
        return true;
    }

    //Transform the BNB from account to the SC
    function transferBNB(uint gameID) payable public returns(bool){
        gameControl[gameID].betAmount += msg.value;
        betBNBamount[msg.sender] = msg.value;
        emit Received(msg.sender, msg.value);
        return true;
    }
    
    //Transform all of the SC's BNB.---for the test deploy.
    function returnToOwner() public{
        payable(owner()).transfer(address(this).balance);
        emit ReturnToOwner(address(this).balance);
    }

    //Return the BNB to the winner, some comision to the owner.
    function withdraw(uint gameID) external payable {
        payable(msg.sender).transfer(gameControl[gameID].betAmount*(100-feePercent)/(100));
        payable(owner()).transfer(gameControl[gameID].betAmount*(feePercent)/(100));
        emit WithDraw(msg.sender, gameControl[gameID].betAmount*(100-feePercent)/(100));
    }
}